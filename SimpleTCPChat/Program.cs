﻿namespace SimpleTCPChat
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;

    class Program
    {
        static void Main(string[] args)
        {
            // Setup the sockets and clients
            var address = new byte[] { 127, 0, 0, 1 };
            var ipAddress = new IPAddress(address);
            var listener = new TcpListener(ipAddress, 0);
            listener.Start();
            var remoteHost = new TcpClient();
            remoteHost.Connect(listener.LocalEndpoint as IPEndPoint);
            var localHost = listener.AcceptTcpClient();

            // Setup the Writers and Readers
            var localWriter = new StreamWriter(localHost.GetStream());
            var localReader = new StreamReader(localHost.GetStream());
            var remoteWriter = new StreamWriter(remoteHost.GetStream());
            var remoteReader = new StreamReader(remoteHost.GetStream());
            remoteWriter.AutoFlush = true;
            localWriter.AutoFlush = true;

            // Write from local to remote
            localWriter.Write("Abc");
            var readFromLocal = String.Empty;

            while (remoteReader.Peek() != -1)
            {
                readFromLocal += Convert.ToChar(remoteReader.Read());
            }

            // Write from remote to local
            remoteWriter.Write("123");
            var readFromRemote = String.Empty;

            while (localReader.Peek() != -1)
            {
                readFromRemote += Convert.ToChar(localReader.Read());
            }

            Console.ReadLine();
        }
    }
}