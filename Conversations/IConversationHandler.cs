﻿namespace Conversations
{
    using System;

    public interface IConversationHandler
    {
        Conversation StartNewConversation(ConversationInfo info);

        event EventHandler<ConversationEventArgs> ConversationRequest;
    }
}