﻿namespace Conversations
{
    using System;
    using System.IO;
    using System.Net.Sockets;

    public class Conversation : IDisposable
    {
        private ConversationInfo info;
        private TcpClient partner;
        private StreamReader reader;
        private StreamWriter writer;

        public Conversation(ConversationInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException(
                    nameof(info),
                    "A conversation cannot be created without this information.");
            }

            partner = new TcpClient();

            try
            {
                partner.Connect(info.PartnerConnection);
            }
            catch (SocketException)
            {
                throw new ArgumentException(
                    nameof(info),
                    "Creating a connection using the specified parameters was not possible.");
            }

            this.info = info;
            this.reader = new StreamReader(this.partner.GetStream());
            this.writer = new StreamWriter(this.partner.GetStream());
            this.writer.AutoFlush = true;
        }

        public void Send(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            this.writer.WriteLine(message);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.reader.Close();
                    this.writer.Close();
                    this.partner.Close();
                }

                this.reader = null;
                this.writer = null;
                this.partner = null;

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}