﻿namespace Conversations
{
    using System;
    using System.Net.Sockets;

    public class ConversationHandler : IConversationHandler
    {
        public event EventHandler<ConversationEventArgs> ConversationRequest;

        public Conversation StartNewConversation(ConversationInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException(
                    nameof(info),
                    "The parameter must not be null. The information is required for creating a connection.");
            }

            return new Conversation(info);
        }
    }
}