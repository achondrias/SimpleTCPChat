﻿namespace Conversations
{
    using System;
    using System.Net;

    public class ConversationInfo
    {
        private IPEndPoint partnerConnectionInfo;

        private string partnerName;

        public IPEndPoint PartnerConnection
        {
            get
            {
                return this.partnerConnectionInfo;
            }
        }

        public string PartnerName
        {
            get
            {
                return partnerName;
            }
        }

        public ConversationInfo(IPEndPoint connectionTarget, string partnerName)
        {
            if (connectionTarget == null)
            {
                throw new ArgumentNullException(
                    nameof(connectionTarget),
                    "A conversation must have a target.");
            }

            if (string.IsNullOrEmpty(partnerName))
            {
                throw new ArgumentNullException(
                    nameof(partnerName),
                    "A name is required for the conversation partner.");
            }

            this.partnerConnectionInfo = connectionTarget;
            this.partnerName = partnerName;
        }
    }
}