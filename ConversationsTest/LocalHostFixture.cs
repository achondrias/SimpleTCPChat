﻿namespace ConversationsTest
{
    using Conversations;
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;

    public class LocalHostFixture : IDisposable
    {
        private TcpClient remoteServer;
        public Conversation LocalHostCommunication { get; private set; }
        public StreamReader RemoteReader { get; private set; }

        public LocalHostFixture()
        {
            var address = new byte[] { 127, 0, 0, 1 };
            var ipAddress = new IPAddress(address);
            var listener = new TcpListener(ipAddress, 0);
            listener.Start();
            var info = new ConversationInfo(listener.LocalEndpoint as IPEndPoint, "Remote server");
            this.LocalHostCommunication = new Conversation(info);
            this.remoteServer = listener.AcceptTcpClient();
            this.RemoteReader = new StreamReader(this.remoteServer.GetStream());
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.LocalHostCommunication.Dispose();
                    this.RemoteReader.Close();
                    this.remoteServer.Close();
                }

                this.LocalHostCommunication = null;
                this.RemoteReader = null;
                this.remoteServer = null;

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}