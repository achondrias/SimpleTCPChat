﻿namespace ConversationsTest
{
    using Xunit;

    public class ConnectionTest : IClassFixture<LocalHostFixture>
    {
        LocalHostFixture fixture;

        public ConnectionTest(LocalHostFixture fixture)
        {
            this.fixture = fixture;
        }

        [Fact]
        public void SendValue()
        {
            string message = "This message was send.";
            string received;

            this.fixture.LocalHostCommunication.Send(message);
            received = this.fixture.RemoteReader.ReadLine();

            Assert.Equal(message, received);
        }
    }
}